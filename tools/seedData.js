import sequelize from '../src/data/sequelize';
import seedData from '../src/data/seeds/seederInitialDatabase';

sequelize.sync({force: true}).then(() => {
    return seedData()
});