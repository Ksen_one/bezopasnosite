import io from 'socket.io-client';
import { onMessageReceived } from '../actions/chatActions';
import { SOCKET_REGISTER, SOCKET_UNREGISTER, SOCKET_SEND_MESSAGE } from '../actions/actionTypes';

export const createSocketMiddleware = (url) => {
    return ({ getState, dispatch }) => {
        let socket = null
        return (next) => (action) => {
            switch (action.type) {
                case SOCKET_REGISTER:
                    socket = io(url, {
                        query: {
                            token: getState().auth.token,
                        },
                    });
                    socket.on('error', (err) => {
                        console.error(err);
                    });
                    socket.on('serverMessage', ({ message }) => {
                        dispatch(onMessageReceived(message));
                    });
                    return;
                case SOCKET_UNREGISTER:
                    if (!socket) return;
                    socket.off('serverMessage');
                    socket = null;
                    return;
                case SOCKET_SEND_MESSAGE:
                    if (!socket) return;
                    socket.emit('clientMessage', {
                        currentRoom: action.payload.currentRoom,
                        message: action.payload.message,
                    });
                    return;
                default:
                    break;
            }

            return next(action);
        };
    };
};


