import {
    NOTIFICATION_SHOW,
    NOTIFICATION_HIDE
} from './actionTypes';

const showNotification = (text, type) => ({
    type: NOTIFICATION_SHOW,
    payload: {
        text,
        type
    }
});

const hideNotification = () => ({
    type: NOTIFICATION_HIDE
})

let timer = null;
export const notification = (text, type) => 
    dispatch => {
        if (timer) clearTimeout(timer);
        dispatch(showNotification(text, type));
        timer = setTimeout(() => dispatch(hideNotification()), 3000);
    }