import { LOAD_USERS_FAILURE, LOAD_USERS_SUCCESS, LOAD_USERS_REQUEST, CREATE_NEW_USER_SUCCESS, DELETE_USER_SUCCESS } from './actionTypes';
import { fetchData } from '../services/fetchService';
import { notification } from './notificationActions';

function requestLoadUsers() {
    return {
        type: LOAD_USERS_REQUEST,
    };
}

function receiveUsers(users) {
    return {
        type: LOAD_USERS_SUCCESS,
        payload: {
            users,
        },
    };
}

function loadUsersError(error) {
    return {
        type: LOAD_USERS_FAILURE,
        payload: {
            error,
        },
    };
}

export function receiveNewUser(user) {
    return {
        type: CREATE_NEW_USER_SUCCESS,
        payload: {
            user,
        },
    };
}

export function removeDeletedUser(user) {
    return {
        type: DELETE_USER_SUCCESS,
        payload: {
            user,
        },
    };
}

export function loadUsers() {
    return async (dispatch, getStore) => {
        dispatch(requestLoadUsers());
        try {
            const { data } = await fetchData('/api/getUsers', {}, getStore().auth.token);
            dispatch(receiveUsers(data));
        } catch (err) {
            dispatch(loadUsersError(err));
        }
    };
}

export function deleteUser(id) {
    return async (dispatch, getStore) => {
        try {
            await fetchData('/api/deleteUser', { id }, getStore().auth.token);
            dispatch(notification('User deleted', 'success'));
        } catch (err) {
            dispatch(notification(err, 'error'));
        }
    };
}

export function updateUserAccessLevel(id, accessLevel) {
    return async (dispatch, getStore) => {
        try {
            await fetchData('/api/updateUserAccessLevel', { id, accessLevel }, getStore().auth.token);
            dispatch(notification('User access level changed', 'success'));
        } catch (err) {
            dispatch(notification(err, 'error'));
        }
    };
}
