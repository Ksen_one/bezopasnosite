import {
    LOAD_FILTERS_SUCCESS,
    LOAD_FILTERS_REQUEST,
    LOAD_FILTERS_ERROR,
    SET_FILTER,
    RESET_FILTER,
} from './actionTypes';
import { getFetchAction } from '../services/getFetchAction';

const loadFiltersRequest = () => ({
    type: LOAD_FILTERS_REQUEST,
});

const loadFiltersSuccess = (data) => ({
    type: LOAD_FILTERS_SUCCESS,
    payload: {
        filters: data,
    },
});
const loadFilterError = (err) => ({
    type: LOAD_FILTERS_ERROR,
    payload: {
        err,
    },
});

export const setFilter = (filter) => ({
    type: SET_FILTER,
    payload: {
        filter,
    },
});
export const resetFilter = () => ({
    type: RESET_FILTER,
});

export const loadFilters = () =>
    getFetchAction('/api/getFilters', loadFiltersRequest, loadFiltersSuccess, loadFilterError)()
