import {
    CREATE_ROOM_REQUEST,
    CREATE_ROOM_SUCCESS,
    CREATE_ROOM_ERROR,
    DELETE_ROOM_REQUEST,
    DELETE_ROOM_SUCCESS,
    DELETE_ROOM_ERROR,
    GET_ALL_ROOMS_REQUEST,
    GET_ALL_ROOMS_SUCCESS,
    GET_ALL_ROOMS_ERROR,
    GET_ALL_MESSAGES_IN_ROOM_REQUEST,
    GET_ALL_MESSAGES_IN_ROOM_SUCCESS,
    GET_ALL_MESSAGES_IN_ROOM_ERROR,
    SOCKET_REGISTER,
    SOCKET_UNREGISTER,
    SOCKET_SEND_MESSAGE,
    SOCKET_MESSAGE_RECEIVED,
    CHANGE_ROOM
} from './actionTypes';
import { getFetchAction } from '../services/getFetchAction';

const createRoomRequest = () => ({
    type: CREATE_ROOM_REQUEST,
});

const createRoomSuccess = (room) => ({
    type: CREATE_ROOM_SUCCESS,
    payload: { room },
});

const createRoomError = (err) => ({
    type: CREATE_ROOM_ERROR,
    payload: { err },
});

const deleteRoomRequest = () => ({
    type: DELETE_ROOM_REQUEST,
});

const deleteRoomSuccess = (roomId) => ({
    type: DELETE_ROOM_SUCCESS,
    payload: { roomId },
});

const deleteRoomError = (err) => ({
    type: DELETE_ROOM_ERROR,
    payload: { err },
});

const getAllRoomsRequest = () => ({
    type: GET_ALL_ROOMS_REQUEST,
});

const getAllRoomsSuccess = (rooms) => ({
    type: GET_ALL_ROOMS_SUCCESS,
    payload: { rooms },
});

const getAllRoomsError = (err) => ({
    type: GET_ALL_ROOMS_ERROR,
    payload: { err },
});

const getAllMessagesInRoomRequest = () => ({
    type: GET_ALL_MESSAGES_IN_ROOM_REQUEST,
});

const getAllMessagesInRoomSuccess = (messages) => ({
    type: GET_ALL_MESSAGES_IN_ROOM_SUCCESS,
    payload: { messages },
});

const getAllMessagesInRoomError = (err) => ({
    type: GET_ALL_MESSAGES_IN_ROOM_ERROR,
    payload: { err },
});

export const createRoom = (roomTitle) =>
    getFetchAction('/api/createRoom', createRoomRequest, createRoomSuccess, createRoomError)({ roomTitle });

export const deleteRoom = (roomId) =>
    getFetchAction('/api/deleteRoom', deleteRoomRequest, () => deleteRoomSuccess(roomId), deleteRoomError)({ roomId });

export const getAllRooms = () =>
    getFetchAction('/api/getAllRooms', getAllRoomsRequest, getAllRoomsSuccess, getAllRoomsError)(null);

export const getAllMessagesInRoom = (roomId) =>
    getFetchAction(
        '/api/getAllmessagesInRoom',
        getAllMessagesInRoomRequest,
        getAllMessagesInRoomSuccess,
        getAllMessagesInRoomError
    )({ roomId });

export const registerSocket = () => ({
    type: SOCKET_REGISTER,
});

export const unregisterSocket = () => ({
    type: SOCKET_UNREGISTER,
});

export const sendMessage = (currentRoom, message) => ({
    type: SOCKET_SEND_MESSAGE,
    payload: { currentRoom, message },
});
export const onMessageReceived = (message) => ({
    type: SOCKET_MESSAGE_RECEIVED,
    payload: { message },
});

export const changeRoom = (roomId) => ({
    type: CHANGE_ROOM,
    payload: {roomId}
})
