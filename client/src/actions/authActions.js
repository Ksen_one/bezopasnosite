import {
    LOGIN_REQUEST,
    LOGIN_FAILURE,
    LOGIN_SUCCESS,
    LOGOUT_REQUEST,
    LOGOUT_SUCCESS,
    GET_CURRENT_USER_DATA_REQUEST,
    GET_CURRENT_USER_DATA_SUCCESS,
    GET_CURRENT_USER_DATA_ERROR,
    REGISTER_REQUEST,
    REGISTER_SUCCESS,
    REGISTER_FAILURE
} from './actionTypes';
import { fetchData } from '../services/fetchService';
import { getFetchAction } from '../services/getFetchAction';
import { notification } from './notificationActions';

function loginRequest() {
    return {
        type: LOGIN_REQUEST,
    };
}

function loginSuccess({ token, user }) {
    return {
        type: LOGIN_SUCCESS,
        payload: {
            token,
            user
        },
    };
}

function loginError(error) {
    return {
        type: LOGIN_FAILURE,
        payload: {
            error,
        },
    };
}

function registerRequest() {
    return {
        type: REGISTER_REQUEST,
    };
}

function registerSuccess() {
    return {
        type: REGISTER_SUCCESS,
    };
}

function registerError(err) {
    return {
        type: REGISTER_FAILURE,
        payload: {
            err,
        },
    };
}

function logoutRequest() {
    return {
        type: LOGOUT_REQUEST,
    };
}

function logoutSuccess() {
    return {
        type: LOGOUT_SUCCESS,
    };
}

export function logout() {
    return dispatch => {
        dispatch(logoutRequest());
        localStorage.removeItem('token');
        dispatch(logoutSuccess());
    };
}

export function login(email, password, silent) {
    return async dispatch => {
        const payload = {
            email,
            password,
        };
        dispatch(loginRequest());
        try {
            const { data } = await fetchData('/api/login', payload)
            dispatch(loginSuccess(data));
            localStorage.setItem('token', data.token);
            if (!silent) { 
                dispatch(notification('Successful login', 'success'));
            }
        } catch(err) {
            dispatch(loginError(err));
            if (!silent) { 
                dispatch(notification(err.error, 'error'));
            }
            throw err;
        };
    }
}

export function register(email, password, username) {
    return async dispatch => {
        const payload = {
            email,
            password,
            username,
        };
        dispatch(registerRequest());
        try {
            await fetchData('/api/register', payload);
            dispatch(registerSuccess());
            dispatch(notification('Successful refistration', 'success'));
            dispatch(login(email, password, true));
        }
        catch (err) {
            dispatch(registerError(err));
            dispatch(notification(err.error, 'error'));
            throw err;
        }
    };
}


const getCurrentUserDataRequest = () => ({
    type: GET_CURRENT_USER_DATA_REQUEST
});

const getCurrentUserDataSuccess = (user) => ({
    type: GET_CURRENT_USER_DATA_SUCCESS,
    payload: {
        user
    }
});

const getCurrentUserDataError = (err) => ({
    type: GET_CURRENT_USER_DATA_ERROR,
    payload: {
        err
    }
});

export const getCurrentUserData = () =>
    getFetchAction('/api/getCurrentUserData', getCurrentUserDataRequest, getCurrentUserDataSuccess, getCurrentUserDataError)();
