import {
    LOAD_VULNERABILITIES_REQUEST,
    LOAD_VULNERABILITIES_SUCCESS,
    LOAD_VULNERABILITIES_ERROR,
    SET_PAGE,
    SET_PAGE_SIZE,
} from './actionTypes';
import { fetchData } from '../services/fetchService';

export const setPage = (page) => ({
    type: SET_PAGE,
    payload: {
        page,
    },
});

export const setPageSize = (pageSize) => ({
    type: SET_PAGE_SIZE,
    payload: {
        pageSize,
    },
});

const loadVulnerabilitiesRequest = () => ({
    type: LOAD_VULNERABILITIES_REQUEST,
});

const loadVulnerabilitiesSuccess = (data) => ({
    type: LOAD_VULNERABILITIES_SUCCESS,
    payload: {
        totalCount: data.count,
        vulnerabilities: data.rows,
    },
});

const loadVulnerabilitiesError = (err) => ({
    type: LOAD_VULNERABILITIES_ERROR,
    payload: {
        err,
    },
});

export function loadVulnerabilitiesWithOptions() {
    return async (dispatch, getStore) => {
        const state = getStore();
        const { page, pageSize } = state.vulnerability;
        const limit = pageSize;
        const offset = (page - 1) * pageSize;

        const filter = state.filter.filter;

        const payload = {
            limit,
            offset,
            filter,
        };
        dispatch(loadVulnerabilitiesRequest());
        try {
            const { data } = await fetchData('/api/getVulnerabilities', payload, getStore().auth.token);
            dispatch(loadVulnerabilitiesSuccess(data));
        } catch (err) {
            dispatch(loadVulnerabilitiesError(err));
            return Promise.reject(err);
        }
    };
}
