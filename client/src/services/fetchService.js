export async function fetchData(url, payload, token) {
    const fetchOptions = {
        method: 'POST',
        headers: {
            Accept: 'application/json, text/plain, */*',
            'content-Type': 'application/json',
        },
        body: payload ? JSON.stringify(payload) : null,
    };
    if (token) {
        fetchOptions.headers.authorization = `Bearer ${token}`;
    }

    try {
        const response = await fetch(url, fetchOptions);
        const data = await handleResponse(response);
        return data;
    } catch (err) {
        console.error(`${err.status} ${err.statusText}: ${err.err}`);
        return Promise.reject(err);
    }
}

function handleResponse(response) {
    let contentType = response.headers.get('content-type');
    if (contentType.includes('application/json')) {
        return handleJSONResponse(response);
    } else if (contentType.includes('text/html')) {
        return handleTextResponse(response);
    } else {
        // Other response types as necessary. I haven't found a need for them yet though.
        throw new Error(`Sorry, content-type ${contentType} not supported`);
    }
}

function handleJSONResponse(response) {
    return response.json().then((json) => {
        if (response.ok) {
            return json;
        } else {
            return Promise.reject({
                ...json,
                status: response.status,
                statusText: response.statusText,
            });
        }
    });
}
function handleTextResponse(response) {
    return response.text().then((text) => {
        if (response.ok) {
            return text;
        } else {
            return Promise.reject({
                err: text,
                status: response.status,
                statusText: response.statusText,
            });
        }
    });
}
