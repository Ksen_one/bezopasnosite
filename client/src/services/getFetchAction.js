import { fetchData } from './fetchService';

export const getFetchAction = (url, requestAction, successAction, errorAction) => (payload) => async (dispatch, getStore) => {
    dispatch(requestAction());
    try {
        const { data } = await fetchData(url, payload, getStore().auth.token);
        dispatch(successAction(data));
    }
    catch (err) {
        dispatch(errorAction(err));
        return Promise.reject(err);
    }
};
