import * as types from '../actions/actionTypes';

const initialState = {
    currentRoom: null,
    rooms: { 
        isFetching: false,
        data: [] 
    },
    messages: {
        isFetching: false,
        data: []
    }
};

export function chatReducer(state = initialState, action) {
    switch (action.type) {
        case types.CREATE_ROOM_REQUEST:
            return {
                ...state,
                rooms: {
                    data: state.rooms.data,
                    isFetching: true,
                }
            };
        case types.CREATE_ROOM_SUCCESS:
            return ({
                ...state,
                rooms: {
                    data: [ ...state.rooms.data, action.payload.room ],
                    isFetching: false
                }
            })
        case types.CREATE_ROOM_ERROR:
            return ({
                ...state,
                rooms: {
                    data: state.rooms.data,
                    isFetching: false,
                    err: action.payload.err
                }
            })
        case types.DELETE_ROOM_REQUEST:
            return ({
                ...state,
                rooms: {
                    data: state.rooms.data,
                    isFetching: true,
                }
            })
        case types.DELETE_ROOM_SUCCESS:
            return ({
                ...state,
                rooms: {
                    data: [ ...state.rooms.data.filter((room) => room.id !== action.payload.roomId)],
                    isFetching: false,
                }
            })
        case types.DELETE_ROOM_ERROR:
            return ({
                ...state,
                rooms: {
                    data: state.rooms.data,
                    isFetching: false,
                    err: action.payload.err
                }
            })
        case types.GET_ALL_ROOMS_REQUEST:
            return ({
                ...state,
                rooms: {
                    data: state.rooms.data,
                    isFetching: true,
                }
            })
        case types.GET_ALL_ROOMS_SUCCESS:
            return ({
                ...state,
                rooms: {
                    data: action.payload.rooms,
                    isFetching: false,
                }
            })
        case types.GET_ALL_ROOMS_ERROR:
            return ({
                ...state,
                rooms: {
                    data: state.rooms.data,
                    isFetching: false,
                    err: action.payload.err
                }
            })
        case types.GET_ALL_MESSAGES_IN_ROOM_REQUEST:
            return ({
                ...state,
                messages: {
                    ...state.messages,
                    isFetching: true
                }
            })
        case types.GET_ALL_MESSAGES_IN_ROOM_SUCCESS:
            return ({
                ...state,
                messages: {
                    data: action.payload.messages,
                    isFetching: false
                }
            })
        case types.GET_ALL_MESSAGES_IN_ROOM_ERROR:
            return ({
                ...state,
                messages: {
                    ...state.messages,
                    isFetching: false,
                    err: action.payload.err
                }
            })
        case types.SOCKET_MESSAGE_RECEIVED:
            return ({
                ...state,
                messages: {
                    isFething: false,
                    data: [...state.messages.data, action.payload.message]
                }
            })
        case types.CHANGE_ROOM:
            return ({
                ...state,
                currentRoom: action.payload.roomId
            })
        default:
            return state;
    }
}
