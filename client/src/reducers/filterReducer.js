import * as types from '../actions/actionTypes';

const initialState = {
    ifFetching: false,
    filter: null,
    filters: [],
};

export function filterReducer(state = initialState, action) {
    switch (action.type) {
        case types.LOAD_FILTERS_REQUEST:
            return {
                ...state,
                ifFetching: true,
                filters: [],
            };
        case types.LOAD_FILTERS_SUCCESS:
            return {
                ...state,
                ifFetching: false,
                filters: action.payload.filters,
            };
        case types.LOAD_FILTERS_ERROR:
            return {
                ...state,
                ifFetching: false,
                err: action.payload.err,
            };
        case types.SET_FILTER:
            return {
                ...state,
                filter: action.payload.filter,
            };
        case types.RESET_FILTER:
            return {
                ...state,
                filter: null,
            };
        default:
            return state;
    }
}
