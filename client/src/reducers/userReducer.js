import * as types from '../actions/actionTypes';

const initialState = {
    isFetching: true,
    users: [],
};

export function userReducer(state = initialState, action) {
    switch (action.type) {
        case types.LOAD_USERS_REQUEST:
            return {
                ...state,
                isFetching: true,
            };
        case types.LOAD_USERS_SUCCESS:
            return {
                ...state,
                users: action.payload.users,
                isFetching: false,
            };
        case types.LOAD_USERS_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: action.payload.error
            };
        case types.CREATE_NEW_USER_SUCCESS:
            return {
                ...state,
                isFetching: false,
                users: [
                    ...state.users,
                    action.payload.user
                ]
            };
        case types.DELETE_USER_SUCCESS:
            return {
                ...state,
                isFetching: false,
                users: state.users.filter((user) => user.id !== action.payload.user.id)
            };
        default:
            return state;
    }
}
