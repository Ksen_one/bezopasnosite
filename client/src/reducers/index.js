import {combineReducers} from 'redux';

import { vulnerabilityReducer } from './vulnerabilityReducer';
import { filterReducer } from './filterReducer';
import { authReducer } from './authReducer';
import { userReducer } from './userReducer';
import { chatReducer } from './chatReducer';
import { notificationReducer } from './notificationReducer';

const rootReducer = combineReducers({
    notifications: notificationReducer,
    vulnerability: vulnerabilityReducer,
    filter: filterReducer,
    auth: authReducer,
    user: userReducer,
    chat: chatReducer
});

export default rootReducer;
