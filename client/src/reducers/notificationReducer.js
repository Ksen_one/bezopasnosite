import * as types from '../actions/actionTypes';

const initialState = {
    history: [],
    show: false,
    notification: null
};

export function notificationReducer(state = initialState, action) {
    switch (action.type) {
        case types.NOTIFICATION_SHOW:
            return {
                history: [ ...state.history, action.payload ],
                show: true,
                notification: {
                    text: action.payload.text,
                    type: action.payload.type
                }
            }
        case types.NOTIFICATION_HIDE:
            return {
                ...state,
                show: false
            }
        default:
            return state;
    }
}
