import * as types from '../actions/actionTypes';

const initialState = {
    isFetching: false,
    isAuthenticated: localStorage.getItem('token') ? true : false,
    token: localStorage.getItem('token'),
    user: null
};

export function authReducer(state = initialState, action) {
    switch (action.type) {
        case types.LOGIN_REQUEST:
            return {
                ...state,
                isFetching: true,
                isAuthenticated: false,
            };
        case types.LOGIN_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isAuthenticated: true,
                token: action.payload.token,
                user: action.payload.user,
                err: '',
            };
        case types.LOGIN_FAILURE:
            return {
                ...state,
                isFetching: false,
                isAuthenticated: false,
                error: action.payload.error,
            };
        case types.LOGOUT_REQUEST:
            return {
                ...state,
            }
        case types.LOGOUT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isAuthenticated: false,
                token: '',
                user: null
            };
        case types.REGISTER_REQUEST: 
            return {
                ...state,
                isFetching: true
            }
        case types.REGISTER_SUCCESS: 
            return {
                ...state,
                isFetching: false
            }
        case types.REGISTER_FAILURE: 
            return {
                ...state,
                isFetching: false,
                err: action.payload.err
            }
        case types.GET_CURRENT_USER_DATA_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case types.GET_CURRENT_USER_DATA_SUCCESS:
            return {
                ...state,
                isFetching: false,
                user: action.payload.user
            }
        case types.GET_CURRENT_USER_DATA_ERROR:
            return {
                ...state,
                isFetching: false,
                err: action.payload.err
            }
        default:
            return state;
    }
}
