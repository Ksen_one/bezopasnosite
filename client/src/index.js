import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { BrowserRouter , Route } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faStroopwafel, faUserTimes, faTerminal, faPlus, faBars, faTimes, faHistory } from '@fortawesome/free-solid-svg-icons';

import thunk from 'redux-thunk';
import { createSocketMiddleware } from './middlewares/socket';
import rootReducer from './reducers';

import App from './components/App';
import UserProvider from './components/UserProvider';
import Nav from './components/Nav';

library.add(faStroopwafel, faUserTimes, faTerminal, faPlus, faBars, faTimes, faHistory);

const socket = createSocketMiddleware('http://localhost:5000');
const store = createStore(rootReducer, {}, applyMiddleware(socket, thunk));

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <UserProvider>
                <Nav />
                <Route path="/" component={App} />
            </UserProvider>
        </BrowserRouter >
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
