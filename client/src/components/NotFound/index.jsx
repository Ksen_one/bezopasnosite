import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './notfound.css';

export default class NotFound extends Component {
    render() {
        return (
            <div className="notfound text-center">
                <div className="card">
                    <h1 className="h3 mb-3 font-weight-normal">
                        Страница не найдена. Вернуться на{' '}
                        <Link to="/">главную</Link>?
                    </h1>
                </div>
            </div>
        );
    }
}
