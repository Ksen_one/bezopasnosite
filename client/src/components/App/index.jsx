import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Vulnerabilities from '../Vulnerabilities';
import { Switch, Route } from 'react-router-dom';
import Chat from '../Chat';
import Admin from '../Admin';
import Signin from '../Signin';
import Signup from '../Signup';
import PrivateRoute from '../PrivateRoute';

class App extends Component {
    render() {
        return (
            <div className="App">
                <main role="main">
                    <Route exact path="/signin" component={Signin} />
                    <Route exact path="/signup" component={Signup} />
                    <PrivateRoute level={1} path="/admin" component={Admin} />
                    <Switch>
                        <PrivateRoute exact path="/vulnerabilities" component={Vulnerabilities} />
                        <PrivateRoute exact path="/chat" component={Chat} />
                    </Switch>
                </main>
            </div>
        );
    }
}

export default App;
