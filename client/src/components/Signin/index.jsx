import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import * as authActions from '../../actions/authActions';
import './signin.css';

class Signin extends Component {
    static propTypes = {
        signin: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',

            error: {
                email: '',
                password: '',
            },
            success: false,
        };
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        if (this.state.error.email || this.state.error.password) {
            this.setState({ error: { ...this.state.error, [name]: '' } });
        }
        const value = e.target.value;
        this.setState({ [name]: value });
    };

    handleSubmit = (e) => {
        e.preventDefault();

        const error = {};
        if (!this.state.email) {
            error.email = 'Enter email';
        } else {
            error.email = this.validateEmail(this.state.email);
        }
        if (!this.state.password) {
            error.password = 'Enter password';
        }
        if (error.email || error.password) {
            this.setState({error: { ...this.state.error, ...error}});
            return;
        }
        this.props
                .signin(this.state.email, this.state.password)
                .then(() => {
                    this.setState({ success: true });
                })
                .catch((err) => this.setState({ error: { email: err.error } }));
    };

    handleFocus = (e) => {
        this.setState({ error: { ...this.state.error, [e.target.name]: '' } });
    };

    handleBlur = (e) => {
        const email = e.target.value;
        if (email) {
            const emailError = this.validateEmail(email);
            if (emailError){
                this.setState({ error: { ...this.state.error, email: emailError } });
            }
        }
    };

    validateEmail = (email) => {
        const emailValid = email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        if (!emailValid) {
            return 'Enter valid email'
        }
        return ''
    }

    render() {
        if (this.state.success) {
            return <Redirect to="/" />;
        }

        const { error } = this.state;
        return (
            <div className="signin text-center">
                <form className="form-signin">
                    <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                    <label htmlFor="inputEmail" className="sr-only">
                        Email address
                    </label>
                    <input
                        type="email"
                        id="inputEmail"
                        name="email"
                        value={this.state.email}
                        className={`form-control ${(error.email || error.password) ? 'is-invalid': ''} ${this.state.success ?
                            'is-valid':''}`}
                        placeholder="Email address"
                        required
                        autoFocus
                        onChange={this.handleUserInput}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                    />

                    <label htmlFor="inputPassword" className="sr-only">
                        Password
                    </label>
                    <input
                        type="password"
                        id="inputPassword"
                        name="password"
                        className={`form-control ${(error.email || error.password) ? 'is-invalid': ''} ${this.state.success ?
                            'is-valid':''}`}
                        placeholder="Password"
                        required
                        onChange={this.handleUserInput}
                        onFocus={this.handleFocus}
                    />
                    <div className="invalid-feedback mb-3">{error.email}</div>
                    <div className="invalid-feedback mb-3">{error.password}</div>
                    <div className="valid-feedback mb-3">Success. Redirect in {this.state.timeToRedirect}</div>

                    <button className="btn btn-lg btn-dark btn-block" type="submit" onClick={this.handleSubmit}>
                        Sign in
                    </button>
                    <Link className="btn btn-sm btn-outline-dark btn-block" to="/signup">
                        Sign up
                    </Link>
                    <p className="mt-5 mb-3 text-muted">
                        <Link to="/">Home</Link> ©2018
                    </p>
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signin: (email, password) => dispatch(authActions.login(email, password)),
    };
};

export default connect(
    () => ({}),
    mapDispatchToProps
)(Signin);
