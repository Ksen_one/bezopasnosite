import React from 'react';
import Result from './Result'
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import Paginator from './Paginator';
import * as vulnerabilitiesActions from '../../actions/vulnerabilitiesActions';
import ColumnSelector from './ColumnSelector';
import _ from 'lodash';

class Table extends React.Component{
    static propTypes = {
        isFetching: PropTypes.bool.isRequired,
        vulnerabilities: PropTypes.array,

        loadVulnerabilities: PropTypes.func.isRequired
    }

    constructor() {
        super();
        this.state = {
            columns: [
                'id',
                'identifier',
                'vulnerabilityName'
            ]
        }
    }

    componentWillMount(){
        this.props.loadVulnerabilities();
    }

    setColumn = (column) => {
        if (!this.state.columns.includes(column)) {
            this.setState({
                columns: [
                    ...this.state.columns,
                    column
                ]
            });
        } else {
            this.setState({
                columns: this.state.columns.filter((col) => col !== column)
            })
        }
    }

    render(){
        return(
            <div> 
                <div className="container-fluid p-0">
                    <div className="row justify-content-center mb-3 mt-3">
                        <div className="col my-2">
                            <ColumnSelector setColumn={ this.setColumn } currentColumns={ this.state.columns } />
                        </div>
                        <div className="col-sm-8 my-2">
                            <Paginator />
                        </div>
                    </div>
                </div>
                
                <table className="table table-striped table-sm">
                    <thead>
                        <tr>
                            { this.state.columns.map((column) => 
                                (<th scope="col" key={ column }>{ _.upperFirst(_.toLower(_.startCase(column))) }</th>)    
                            )}
                        </tr>
                    </thead>
                    <tbody>
                        { this.props.vulnerabilities.map((item) => {
                            return (<Result key={item.id} item={item} columns={ this.state.columns }/>);
                        }) }
                    </tbody>
                </table>
                
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isFetching: state.vulnerability.isFetching, 
        vulnerabilities: state.vulnerability.vulnerabilities,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        loadVulnerabilities: () => {
            dispatch(vulnerabilitiesActions.loadVulnerabilitiesWithOptions());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Table);
