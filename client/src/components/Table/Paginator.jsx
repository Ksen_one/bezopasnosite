import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as vulnerabilitiesActions from '../../actions/vulnerabilitiesActions';

class Paginator extends React.Component {
    static propTypes = {
        page: PropTypes.number.isRequired,
        pageSize: PropTypes.number.isRequired,
        totalCount: PropTypes.number.isRequired,

        setPage: PropTypes.func.isRequired,
        setPageSize: PropTypes.func.isRequired,
    }

    constructor() {
        super();
        this.state = {
            pagesCount: 0
        }
    }

    goToHandler = (event) => {
        if (event.key === 'Enter') {
            const { pageSize, totalCount } = this.props;
            if (event.target.value < 1 || event.target.value > Math.ceil(totalCount / pageSize)) {
                return;
            }
            this.props.setPage(+event.target.value); 
        }
    }

    render() {
        const availablePageSizes = [10,20,30,50,100];
        const { pageSize, page, totalCount } = this.props;
        const pagesCount = Math.ceil(totalCount / pageSize);
        return (
            <div className="input-group input-group-sm">
                <div className="input-group-prepend">
                    <span className="input-group-text">{this.props.page} of {pagesCount}</span>
                    <select className="form-control form-control-sm input-group-text" value={pageSize} onChange={this.props.setPageSize}>
                        {availablePageSizes.map((size) => <option key={size}>{size}</option>)}
                    </select>
                    <button className="btn btn-outline-dark" type="button" onClick={() => { this.props.setPage(1); }} disabled={page === 1}>{'<<'}</button>
                    <button className="btn btn-outline-dark" type="button" onClick={() => { this.props.setPage(page-1); }} disabled={page === 1}>{'<'}</button>
                </div>
                <div className="input-group input-group-sm" style={{width: '50px'}}>
                    <input type="text" className="rounded-0 form-control" onKeyPress={this.goToHandler} />
                </div>
                <div className="input-group-append" id="button-addon4">
                    <button className="btn btn-outline-dark" type="button" onClick={() => { this.props.setPage(page+1); }}>{'>'}</button>
                    <button className="btn btn-outline-dark" type="button" onClick={() => { this.props.setPage(pagesCount); }}>{'>>'}</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        page: state.vulnerability.page,
        pageSize: state.vulnerability.pageSize,
        totalCount: state.vulnerability.totalCount
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setPage: (page) => {
            dispatch(vulnerabilitiesActions.setPage(page));
            dispatch(vulnerabilitiesActions.loadVulnerabilitiesWithOptions());
        },
        setPageSize: (event) => {
            dispatch(vulnerabilitiesActions.setPageSize(+event.target.value));
            dispatch(vulnerabilitiesActions.loadVulnerabilitiesWithOptions());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Paginator);