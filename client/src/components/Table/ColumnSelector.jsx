import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import _ from 'lodash';

class ColumnSelector extends React.Component{
    static propTypes = {
        availableColumns: PropTypes.array.isRequired,

        setColumn: PropTypes.func.isRequired,
        currentColumns: PropTypes.array.isRequired
    }

    constructor() {
        super();
        this.state = {
            open: false
        }
    }

    render(){
        return(
            <div className="dropdown">
                <button className="btn btn-dark btn-sm" type="button" onClick={ () => { this.setState({ open: !this.state.open })}}>
                    Select columns
                </button>
                <div className="dropdown-menu p-2" style={{display: this.state.open ? 'inline-block' : 'none' }}>
                    {this.props.availableColumns.map((column) => {
                        return (
                            <div className="form-check" key={column} style={{whiteSpace: 'nowrap'}}>
                                <input className="form-check-input" type="checkbox" checked={ this.props.currentColumns.includes(column) } onChange={ () => { this.props.setColumn(column); } }/>
                                <label className="form-check-label">
                                    { _.upperFirst(_.toLower(_.startCase(column))) }
                                </label>
                            </div>
                        )
                    })}
                </div>
            </div>
        );
    }
}

export default connect((state) => ({ availableColumns: state.filter.filters }))(ColumnSelector);
