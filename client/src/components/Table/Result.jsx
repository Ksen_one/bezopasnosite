import React from 'react';
import PropTypes from "prop-types";
import "./Results.css"

class Result extends React.Component{
    static propTypes = {
        item: PropTypes.object,
        columns: PropTypes.array
    }

    render(){
        return(
            <tr >
                {this.props.columns.map((column) => 
                    (<td key={column}>
                        {this.props.item[column]}
                    </td>)    
                )}
            </tr>
        );
    }
}

export default Result;