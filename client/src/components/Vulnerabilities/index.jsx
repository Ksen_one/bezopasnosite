import React from 'react';
import Filters from '../Filters/Filters';
import Table from '../Table';

const Vulnerabilities = () => (
    <div className="container-fluid">
        <div className="row">
            <nav className="col-md-2 p-0 h-100">
                <Filters />
            </nav>
            <div className="col-md-9 ml-sm-auto col-lg-10 px-4">
                <Table />
            </div>
        </div>
    </div>
);

export default Vulnerabilities;
