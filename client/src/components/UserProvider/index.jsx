import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCurrentUserData } from '../../actions/authActions';
import './userProvider.css';

class UserProvider extends Component {
    constructor(props) {
        super(props);
        const { isAuthenticated, user } = this.props;

        if (isAuthenticated && !user) {
            this.props.getUserData()
        }
    }

    render() {
        const { children, isAuthenticated, user } = this.props;
        if (isAuthenticated && !user) {
            return (
                <div className="text-center">
                    <div className="loader d-inline-block" style={{marginTop: '35%'}} />
                </div>
            );
        }
        return <div>{children}</div>;
    }
}

const mapStoreToProps = (store) => {
    const { isAuthenticated, user } = store.auth;
    return {
        isAuthenticated,
        user,
    };
};

const mapDispatchToProps = (dispatch) => ({
    getUserData: () => dispatch(getCurrentUserData()),
});

export default connect(
    mapStoreToProps,
    mapDispatchToProps,
    null,
    { pure: false }
)(UserProvider);
