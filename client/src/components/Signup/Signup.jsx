import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './signup.css';
import * as authActions from '../../actions/authActions';

class Signup extends Component {
    static propTypes = {
        signup: PropTypes.func.isRequired,

        home: PropTypes.bool,
        signin: PropTypes.bool,
        confirmText: PropTypes.string,
        headerText: PropTypes.string,
        callback: PropTypes.func,
    };

    static defaultProps = {
        home: false,
        signin: false,
        confirmText: 'Sign up',
        headerText: 'Sign up',
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            username: '',
            password: '',
            confirmPassword: '',
            formErrors: {
                email: '',
                password: '',
                confirmPassword: '',
                username: '',
            },
            emailValid: false,
            confirmPasswordValid: false,
            passwordValid: false,
            formValid: false,
            usernameValid: false,

            success: false,
        };
    }

    validateField(fieldName, value, cb) {
        let formErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let confirmPasswordValid = this.state.confirmPasswordValid;
        let usernameValid = this.state.usernameValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                formErrors.email = emailValid ? '' : ' is invalid';
                break;
            case 'username':
                usernameValid = value.length >= 3;
                formErrors.username = usernameValid ? '' : ' is too short';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                formErrors.password = passwordValid ? '' : ' is too short';
                break;
            case 'confirmPassword':
                if (value === '') return;
                confirmPasswordValid = value === this.state.password;
                formErrors.confirmPassword = confirmPasswordValid ? '' : ' didnt match';
                break;
            default:
                break;
        }
        this.setState(
            {
                formErrors,
                emailValid,
                passwordValid,
                confirmPasswordValid,
                usernameValid,
            },
            () => {
                this.validateForm(cb);
            }
        );
    }

    validateForm(cb) {
        this.setState(
            {
                formValid:
                    this.state.emailValid &&
                    this.state.passwordValid &&
                    this.state.confirmPasswordValid &&
                    this.state.usernameValid,
            },
            cb
        );
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value, formValid: false });
    };

    handleBlur = e => {
        const name = e.target.name;
        const value = e.target.value;

        const newState = { [name]: value };
        let withValidate = true;

        if (!value) {
            withValidate = false;
        }

        this.setState(newState, () => {
            if (withValidate) {
                this.validateField(name, value, () => {
                    if (name === 'password') {
                        if (this.state.confirmPassword) {
                            this.validateField('confirmPassword', this.state.confirmPassword);
                        }
                    }
                });
            }
        });
    };

    handleFocus = e => {
        const name = e.target.name;

        const newState = {
            [`${name}Valid`]: false,
            formErrors: {
                ...this.state.formErrors,
                [name]: '',
            },
        };
        if (name === 'password') {
            newState.confirmPasswordValid = false;
            newState.formErrors.confirmPassword = '';
        }

        this.setState(newState);
    };

    submitForm = e => {
        e.preventDefault();
        const { email, password, username } = this.state;
        if (this.state.formValid) {
            this.props.signup(email, password, username).then(( data ) => {
                if (data.status === 'success') {
                    this.setState({ success: true });
                }
                this.props.callback && this.props.callback(data);
            });
        }
    };

    render() {
        return (
            <div className="signup text-center">
                <form className="form-signup" onSubmit={this.submitForm}>
                    <h1 className="h3 mb-3 font-weight-normal">{this.props.headerText}</h1>
                    <label htmlFor="inputEmail" className="sr-only">
                        Email address
                    </label>
                    <input
                        id="inputEmail"
                        type="email"
                        value={this.state.email}
                        onChange={this.handleUserInput}
                        onBlur={this.handleBlur}
                        onFocus={this.handleFocus}
                        className={`mt-1 form-control ${
                            this.state.emailValid
                                ? 'is-valid'
                                : this.state.formErrors.email && (!this.state.emailValid && 'is-invalid')
                        }`}
                        placeholder="Email address"
                        required
                        autoFocus
                        name="email"
                    />
                    <div className="invalid-feedback">{this.state.formErrors.email}</div>

                    <label htmlFor="inputUsername" className="sr-only">
                        Username
                    </label>
                    <div className="mt-1 input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="inputGroupPrepend2">
                                @
                            </span>
                        </div>
                        <input
                            type="text"
                            name="username"
                            value={this.state.username}
                            onChange={this.handleUserInput}
                            onBlur={this.handleBlur}
                            onFocus={this.handleFocus}
                            className={`form-control ${
                                this.state.usernameValid
                                    ? 'is-valid'
                                    : this.state.formErrors.username && (!this.state.usernameValid && 'is-invalid')
                            }`}
                            id="inputUsername"
                            placeholder="Username"
                            required
                        />
                        <div className="invalid-feedback">{this.state.formErrors.username}</div>
                    </div>

                    <label htmlFor="inputPassword" className="sr-only">
                        Password
                    </label>
                    <input
                        id="inputPassword"
                        type="password"
                        value={this.state.password}
                        onChange={this.handleUserInput}
                        onBlur={this.handleBlur}
                        onFocus={this.handleFocus}
                        className={`mt-1 form-control ${
                            this.state.passwordValid
                                ? 'is-valid'
                                : this.state.formErrors.password && (!this.state.passwordValid && 'is-invalid')
                        }`}
                        placeholder="Password"
                        required
                        name="password"
                    />
                    <div className="invalid-feedback">{this.state.formErrors.password}</div>

                    <label htmlFor="inputConfirmPassword" className="sr-only">
                        Confirm password
                    </label>
                    <input
                        id="inputConfirmPassword"
                        type="password"
                        value={this.state.confirmPassword}
                        onChange={this.handleUserInput}
                        onBlur={this.handleBlur}
                        onFocus={this.handleFocus}
                        className={`mt-1 form-control ${
                            this.state.confirmPasswordValid
                                ? 'is-valid'
                                : this.state.formErrors.confirmPassword &&
                                  (!this.state.confirmPasswordValid && 'is-invalid')
                        }`}
                        placeholder="Confirm password"
                        required
                        name="confirmPassword"
                    />
                    <div className="invalid-feedback">{this.state.formErrors.confirmPassword}</div>

                    <button
                        className="mt-1 btn btn-lg btn-primary btn-block"
                        type="submit"
                        disabled={!this.state.formValid}>
                        {this.props.confirmText}
                    </button>
                    {this.props.signin && (
                        <Link className="btn btn-sm btn-outline-dark btn-block" to="/signin">
                            Sign in
                        </Link>
                    )}
                    {this.props.home && (
                        <p className="mt-5 mb-3 text-muted">
                            <Link to="/">Home</Link>
                        </p>
                    )}
                </form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        signup: (email, password, username) => dispatch(authActions.register(email, password, username)),
    };
};

export default connect(
    () => ({}),
    mapDispatchToProps
)(Signup);
