import React from 'react';
import Signup from './Signup';

const SignUpPage = () => {
    return (
        <Signup home signin />
    )
}

export default SignUpPage;
