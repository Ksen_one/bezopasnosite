import './Filters.css';
import PropTypes from 'prop-types';
import React from 'react';
import Search from './Items/Search';
import { connect } from 'react-redux';
import * as vulnActions from '../../actions/vulnerabilitiesActions';
import * as filtersActions from '../../actions/filtersActions';

class Filters extends React.Component {
    static propTypes = {
        loadFiltersPropertiesNames: PropTypes.func.isRequired,
        setFilter: PropTypes.func.isRequired,
        resetFilter: PropTypes.func.isRequired,
        filters: PropTypes.array,
    };

    constructor() {
        super();
        this.state = {
            filter: {},
        };
    }

    componentWillMount() {
        this.props.loadFiltersPropertiesNames();
    }

    setFilterHandler = (propertyName, value) => {
        const oldFilter = this.state.filter;
        this.setState(
            { filter: { ...oldFilter, [propertyName]: value } },
            this.setFilter
        );
    };

    setFilter = () => {
        this.props.setFilter(this.state.filter);
    };

    render() {
        return (
            <div className="filter-sidebar">
                <div className="filter-sidebar-sticky">
                    <ul className="nav flex-column">
                        {this.props.filters &&
                            this.props.filters.map(filter => {
                                return (
                                    <li className="nav-item" key={filter}>
                                        <Search
                                            className="nav-link"
                                            propertyName={filter}
                                            onSet={this.setFilterHandler}
                                            setFilter={this.setFilter}
                                            currentFilter={this.state.filter}
                                        />
                                    </li>
                                );
                            })}
                        <li className="nav-item text-center p-4">
                            <button
                                type="button"
                                className="btn btn-danger"
                                onClick={() => {
                                    this.props.resetFilter();
                                }}
                            >
                                Сброс
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        loadFiltersPropertiesNames: () => {
            dispatch(filtersActions.loadFilters());
        },
        setFilter: filter => {
            dispatch(filtersActions.setFilter(filter));
            dispatch(vulnActions.setPage(1));
            dispatch(vulnActions.loadVulnerabilitiesWithOptions());
        },
        resetFilter: () => {
            dispatch(filtersActions.resetFilter());
            dispatch(vulnActions.setPage(1));
            dispatch(vulnActions.loadVulnerabilitiesWithOptions());
        },
    };
};

const mapStateToProps = state => {
    return {
        filters: state.filter.filters,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Filters);
