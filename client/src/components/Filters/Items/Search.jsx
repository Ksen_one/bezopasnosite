import React from 'react';
import PropTypes from "prop-types";
import _ from 'lodash';
import 'font-awesome/css/font-awesome.min.css';
import '../Filters.css'

class Search extends React.Component{
    static propTypes = {
        propertyName: PropTypes.string.isRequired,
        onSet: PropTypes.func.isRequired,
        currentFilter: PropTypes.object
    }

    keyPressHandler = (event) => {
        if (event.key === 'Enter') {
            const value = event.target.value;
            if (value !== '') {
                this.props.onSet(this.props.propertyName, event.target.value);
            } else {
                if (this.props.currentFilter[this.props.propertyName]) {
                    this.props.onSet(this.props.propertyName, '');
                }
            }
        }
    }
    blurHandler = (event) => {
        const value = event.target.value;
        if (value !== '') {
            if (value !== this.props.currentFilter[this.props.propertyName]) {
                this.props.onSet(this.props.propertyName, event.target.value);
            }
        } else {
            if (this.props.currentFilter[this.props.propertyName]) {
                this.props.onSet(this.props.propertyName, '');
            }
        }
    }


    render(){
        return(
            <div className={this.props.className}>
                <div className="input-group input-group-sm">
                    <input type="text" className="form-control" placeholder={ _.upperFirst(_.toLower(_.startCase(this.props.propertyName))) } onBlur={this.blurHandler} onKeyPress={this.keyPressHandler} />
                </div>
            </div>
        );
    }
}

export default Search;