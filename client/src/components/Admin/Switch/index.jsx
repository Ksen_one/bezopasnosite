import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './switch.css';

export default class Switch extends Component {
    static propTypes = {
        enable: PropTypes.bool,
        leftText: PropTypes.string,
        rightText: PropTypes.string,
        onChange: PropTypes.func
    };

    static defaultProps = {
        enable: false,
    };

    constructor(props) {
        super(props);
        this.state = {
            enable: props.enable,
        };
    }

    handleSwitch = () => {
        this.setState({ enable: !this.state.enable }, () => this.props.onChange && this.props.onChange(this.state.enable));
    };

    render() {
        const { leftText, rightText } = this.props;
        return (
            <div className="btn-group btn-group-sm switch" role="group">
                <button
                    type="button"
                    className={`btn ${
                        !this.state.enable ? 'btn-outline-dark' : 'btn-dark'
                    }`}
                    onClick={this.handleSwitch}
                >
                    {leftText}
                </button>
                <button
                    type="button"
                    className={`btn ${
                        this.state.enable ? 'btn-outline-dark' : 'btn-dark'
                    }`}
                    onClick={this.handleSwitch}
                >
                    {rightText}
                </button>
            </div>
        );
    }
}
