import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './admin.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Sidebar from './Sidebar';
import * as userActions from '../../actions/userActions';
import Modal from '../Modal/index';
import Signup from '../Signup/Signup';
import Switch from './Switch';

class Admin extends Component {
    static propTypes = {
        users: PropTypes.array,

        loadUsers: PropTypes.func,
        deleteUser: PropTypes.func,
        updateUserAccessLevel: PropTypes.func,
        recieveNewUser: PropTypes.func,
        removeDeletedUser: PropTypes.func,
    };

    componentWillMount() {
        this.props.loadUsers();
    }

    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="row">
                        <Sidebar />

                        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
                                <h1 className="h2">Users</h1>
                                <div className="btn-toolbar mb-2 mb-md-0">
                                    <div className="btn-group mr-2">
                                        <Modal
                                            className="btn btn-sm btn-success"
                                            component={Signup}
                                            confirmText="Create user"
                                            headerText="Create user"
                                            callback={({ data: user }) => this.props.recieveNewUser(user)}>
                                            New user
                                        </Modal>
                                    </div>
                                </div>
                            </div>

                            <table className="table table-sm">
                                <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">email</th>
                                        <th scope="col">username</th>
                                        <th scope="col">access level</th>
                                        <th scope="col" />
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.users.map(user => {
                                        return (
                                            <tr key={user.id}>
                                                <td>{user.id}</td>
                                                <td>{user.email}</td>
                                                <td>{user.username}</td>
                                                <td>
                                                    <Switch
                                                        leftText={'Admin'}
                                                        rightText={'User'}
                                                        enable={user.accessLevel === '1'}
                                                        onChange={state => {
                                                            this.props.updateUserAccessLevel(user.id, +state);
                                                        }}
                                                    />
                                                </td>
                                                <td>
                                                    <button
                                                        type="button"
                                                        className="btn btn-sm btn-outline-dark"
                                                        onClick={() => {
                                                            this.props
                                                                .deleteUser(user.id)
                                                                .then(() => this.props.removeDeletedUser(user));
                                                        }}>
                                                        <FontAwesomeIcon icon="user-times" />
                                                    </button>
                                                </td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        users: state.user.users,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadUsers: () => dispatch(userActions.loadUsers()),
        recieveNewUser: user => dispatch(userActions.receiveNewUser(user)),
        removeDeletedUser: user => dispatch(userActions.removeDeletedUser(user)),
        deleteUser: id => dispatch(userActions.deleteUser(id)),
        updateUserAccessLevel: (id, accessLevel) => dispatch(userActions.updateUserAccessLevel(id, accessLevel)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Admin);
