import React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import Unauth from '../Unauth';

const PrivateRoute = ({ component: Component, isAuthenticated, user, level = 0, ...rest }) => {
    return (
        <Route
            {...rest}
            render={({ ...props }) => (isAuthenticated && user.accessLevel >= level ? <Component {...props} /> : <Unauth />)}
        />
    );
};

export default connect(
    (store) => ({ isAuthenticated: store.auth.isAuthenticated, user: store.auth.user }),
    null,
    null,
    { pure: false }
)(PrivateRoute);
