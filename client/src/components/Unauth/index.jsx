import React from 'react';
import { Link } from 'react-router-dom';
import './unauth.css';

const Unauth = () => (
    <div className="unauth text-center">
        <div className="card">
            <h1 className="h3 mb-3 font-weight-normal">Необходима авторизация.</h1>
            <Link className="btn btn-primary btn-block" to="/signin">
                Sign in
            </Link>
            <Link className="btn btn-sm btn-outline-dark btn-block" to="/signup">
                Sign up
            </Link>
        </div>
    </div>
);

export default Unauth;
