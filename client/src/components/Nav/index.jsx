import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import './nav.css';
import * as authActions from '../../actions/authActions';

const ListItemLink = ({ to, ...rest }) => (
    <Route
        path={to}
        children={({ match }) => (
            <li className={`nav-item${match ? ' active' : ''}`}>
                <Link to={to} {...rest} />
            </li>
        )}
    />
);

class Nav extends Component {
    static propTypes = {
        isAuthenticated: PropTypes.bool.isRequired,
        signout: PropTypes.func.isRequired,
        user: PropTypes.object,
        notifications: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            showNotificationHistory: false
        };
    }

    toggle = (show) => this.setState({ show });

    render() {
        const { isAuthenticated, signout, user, notifications } = this.props;

        let navbarClass = 'bg-dark';
        let notification = null;
        
        if (notifications.show) {
            notification = notifications.notification;
            switch (notification.type) {
                case 'error':
                    navbarClass = 'bg-danger';
                    break;
                case 'warning':
                    navbarClass = 'bg-dark';
                    break;
                case 'success':
                    navbarClass = 'bg-success';
                    break;
                default:
                    break;
            }
        }

        return (
            <nav className={`navbar navbar-expand-md navbar-dark ${navbarClass} fixed-top flex-md-nowrap p-0 shadow`}>
                <Link className="navbar-brand col-4 col-sm-3 col-md-2 mr-auto" to="/">
                    cloud custom
                </Link>
                <button className="navbar-toggler border-0" type="button" onClick={() => this.toggle(!this.state.show)}>
                    <span className="navbar-toggler-icon" />
                </button>
                <div className={`collapse navbar-collapse ${this.state.show && 'show'}`} >
                    {notifications.show && notification && 
                        <span className="navbar-text mx-2">
                            { notification.text }
                        </span>
                    }
                    {isAuthenticated && (
                        <ul className="navbar-nav ml-md-auto ml-2">
                            <ListItemLink to="/vulnerabilities" className="nav-link" onClick={() => this.toggle(false)}>
                                Vulnerabilities
                            </ListItemLink>
                            <ListItemLink to="/chat" className="nav-link" onClick={() => this.toggle(false)}>
                                Chat
                            </ListItemLink>
                            {+user.accessLevel === 1 && (
                                <ListItemLink to="/admin" className="nav-link" onClick={() => this.toggle(false)}>
                                    Admin
                                </ListItemLink>
                            )}
                        </ul>
                    )}
                    <form className={`form-inline ${!isAuthenticated && 'ml-md-auto'}`}>
                        {isAuthenticated ? (
                            <div className="btn-group mx-2" role="group">
                                <button
                                    className="btn btn-sm btn-outline-light my-2 text-light"
                                    type="button"
                                    disabled>
                                    @{user.username}
                                </button>
                                <button
                                    className="btn btn-sm btn-outline-light my-2"
                                    type="button"
                                    onClick={signout}>
                                    Sign out
                                </button>
                            </div>
                        ) : (
                            <div className="btn-group mx-2" role="group">
                                <Link
                                    className="btn btn-sm btn-light my-2"
                                    role="button"
                                    to="/signin"
                                    onClick={() => this.toggle(false)}>
                                    Sign in
                                </Link>
                                <Link
                                    className="btn btn-sm btn-outline-light my-2"
                                    role="button"
                                    to="/signup"
                                    onClick={() => this.toggle(false)}>
                                    Sign up
                                </Link>
                            </div>
                        )}
                    </form>
                </div>
            </nav>
        );
    }
}

export default connect(
    (state) => ({
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user,
        notifications: state.notifications
    }),
    (dispatch) => ({
        signout: () => dispatch(authActions.logout()),
    }),
    null,
    { pure: false }
)(Nav);
