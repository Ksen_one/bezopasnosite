import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as chatActions from '../../actions/chatActions';

class NewRoom extends Component {
    static propTypes = {
        createRoom: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            roomTitle: '',
            creationMode: false,
        };
    }

    createNewRoom = () => {
        const { roomTitle } = this.state;
        if (roomTitle){
            this.props.createRoom(roomTitle);
            this.cancel();
        }
        // TODO: need to handle success and errors
    };

    open = () => {
        this.setState({ creationMode: true });
    }

    cancel = () => {
        this.setState({ creationMode: false, newRoomTitle: '' });
    };

    handleUserInput = (e) => {
        const { value, name } = e.target;
        this.setState({ [name]: value });
    };

    render() {
        if (this.state.creationMode) {
            return (
                <div className="mt-auto p-0 d-flex justify-content-between">
                    <div className="input-group input-group-lg">
                        <input
                            type="text"
                            style={{ borderTopLeftRadius: 0 }}
                            className="form-control border-dark border-left-0 border-right-0 border-bottom-0"
                            placeholder="New room title"
                            name="roomTitle"
                            value={this.state.roomTitle}
                            onChange={this.handleUserInput}
                        />
                        <div className="input-group-append">
                            <button
                                className="btn btn-outline-success border-bottom-0"
                                type="button"
                                onClick={this.createNewRoom}>
                                Create
                            </button>
                            <button
                                style={{ borderRadius: 0 }}
                                className="btn btn-outline-danger border-bottom-0 border-right-0"
                                type="button"
                                onClick={this.cancel}>
                                Cancel
                            </button>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="mt-auto p-0 d-flex justify-content-between">
                    <div className="input-group input-group-lg">
                        <button
                            type="button"
                            style={{ height: '48px' }}
                            className="btn btn-outline-dark btn-lg btn-block bl border-left-0 border-bottom-0 border-right-0"
                            onClick={this.open}>
                            <FontAwesomeIcon icon="plus" />
                            <span> Create new room</span>
                        </button>
                    </div>
                </div>
            );
        }
    }
}

export default connect(
    null,
    (dispatch) => ({
        createRoom: (roomTitle) => dispatch(chatActions.createRoom(roomTitle)),
    })
)(NewRoom);
