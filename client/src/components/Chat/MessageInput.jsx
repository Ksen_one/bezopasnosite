import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { sendMessage } from '../../actions/chatActions';

class MessageInput extends Component {
    static propTypes = {
        sendMessage: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            message: '',
        };
    }

    sendMessage = () => {
        const { message } = this.state;
        const { currentRoom } = this.props;
        if (!currentRoom || !message) return;
        this.props.sendMessage(currentRoom, message);
        this.setState({ message: '' });
    };

    handleUserInput = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value,
        });
    };

    handleKeys = (e) => {
        if (e.key === 'Enter') {
            this.sendMessage(e);
        }
    };

    render() {
        return (
            <div className="mt-auto p-0">
                <div className="input-group input-group-lg">
                    <input
                        style={{ borderBottom: 0, borderLeft: 0 }}
                        type="text"
                        className="form-control rounded-0 border-dark border-top"
                        placeholder="Your message"
                        name="message"
                        value={this.state.message}
                        onKeyPress={this.handleKeys}
                        onChange={this.handleUserInput}
                    />
                    <div className="input-group-append">
                        <button
                            className="btn btn-outline-dark border-bottom-0 border-right-0 br"
                            type="button"
                            onClick={this.sendMessage}>
                            <FontAwesomeIcon icon="terminal" />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    (store) => ({ currentRoom: store.chat.currentRoom }),
    (dispatch) => ({
        sendMessage: (currentRoom, message) => dispatch(sendMessage(currentRoom, message)),
    })
)(MessageInput);
