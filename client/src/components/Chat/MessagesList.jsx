import React, { Component } from 'react';
import { connect } from 'react-redux';

class MessageList extends Component {
    componentDidUpdate() {
        this.scrollToBottom(false);
    };

    scrollToBottom = (smooth) => {
        this.messagesEnd.scrollIntoView({ behavior: smooth ? 'smooth' : 'instant' });
    };

    render() {
        const { messages } = this.props;
        return (
            <div className="card-body text-dark" style={{ height: '600px', overflowY: 'scroll' }}>
                {!messages || !messages.length > 0 ? (
                    <div className="text-center">No messages.</div>
                ) : (
                    messages.map((message) => (
                        <div key={message.id} className="my-3">
                            <p className="card-title h-6 mb-0">
                                <small className="text-muted">{`@${message.User.username} at ${new Date(
                                    message.date
                                ).toLocaleTimeString()}:`}</small>
                            </p>
                            <p className="card-text">{message.text}</p>
                        </div>
                    ))
                )}
                <div
                    style={{ float: 'left', clear: 'both' }}
                    ref={(el) => {
                        this.messagesEnd = el;
                    }}
                />
            </div>
        );
    }
}
const storeToProps = (store) => ({
    messages: store.chat.messages.data,
});

export default connect(storeToProps)(MessageList);
