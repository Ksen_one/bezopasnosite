import React, { Component } from 'react';
import { connect } from 'react-redux';
import RoomListItem from './RoomListItem';
import { Search } from './Search';
import NewRoom from './NewRoom';
import { getAllRooms } from '../../actions/chatActions';

class RoomList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: '',
        };
    }
    componentWillMount() {
        this.props.getAllRooms();
    }

    handleUserInput = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value,
        });
    };
    render() {
        const { rooms } = this.props;
        return (
            <div className="card border-dark flex-grow-0" style={{ flexBasis: '33%' }}>
                <Search filter={this.state.filter} handleUserInput={this.handleUserInput} />
                <ul
                    className="list-group list-group-flush text-dark"
                    style={{ maxHeight: `${600 - 48}px`, overflowY: 'auto' }}>
                    {rooms &&
                        rooms.length > 0 &&
                        rooms
                            .filter((room) => room.title.includes(this.state.filter))
                            .map((room) => <RoomListItem key={room.id} id={room.id} title={room.title} />)}
                </ul>

                <NewRoom />
            </div>
        );
    }
}

const storeToProps = (store) => ({
    rooms: store.chat.rooms.data,
});

const mapDispatchToProps = (dispatch) => ({
    getAllRooms: () => dispatch(getAllRooms()),
});

export default connect(
    storeToProps,
    mapDispatchToProps
)(RoomList);
