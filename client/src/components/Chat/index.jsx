import React, { Component } from 'react';
import { connect } from 'react-redux';
import './chat.css';
import { registerSocket, unregisterSocket } from '../../actions/chatActions';

import RoomList from './RoomList';
import MessagesList from './MessagesList';
import MessageInput from './MessageInput';

class Chat extends Component {
    componentWillMount() {
        this.props.registerSocket();
    }

    componentWillUnmount() {
        this.props.unregisterSocket();
    }

    startStopRotate = () => {
        this.setState({ spiner: !this.state.spiner });
    };

    render() {
        return (
            <div className={`chat container mt-4`}>
                <div className="card-group row">
                    <RoomList />
                    <div className="card border-dark">
                        <MessagesList />
                        <MessageInput />
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    registerSocket: () => dispatch(registerSocket()),
    unregisterSocket: () => dispatch(unregisterSocket()),
});

export default connect(
    null,
    mapDispatchToProps
)(Chat);
