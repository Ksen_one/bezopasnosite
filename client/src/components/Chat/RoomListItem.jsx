import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as chatActions from '../../actions/chatActions';

class RoomListItem extends Component {
    changeRoom = () => {
        this.props.chamgeRoom(this.props.id);
        this.props.getAllMessagesInRoom(this.props.id);
    };
    render() {
        const { id, title } = this.props;
        const active = this.props.currentRoom === id;
        return (
            <li className={`list-group-item ${active ? 'text-white bg-dark' : ''}`} onClick={this.changeRoom}>
                <span>{title}</span>
                {!active && (
                    <button
                        type="button"
                        aria-label="Close"
                        className="close float-right "
                        style={{ lineHeight: '1rem' }}
                        onClick={(e) => {
                            e.stopPropagation();
                            this.props.deleteRoom(id);
                        }}>
                        <span className="text-dark" >
                            <FontAwesomeIcon icon="times" size="xs" />
                        </span>
                    </button>
                )}
            </li>
        );
    }
}

export default connect(
    (store) => ({
        currentRoom: store.chat.currentRoom,
    }),
    (dispatch) => ({
        deleteRoom: (roomId) => dispatch(chatActions.deleteRoom(roomId)),
        chamgeRoom: (roomId) => dispatch(chatActions.changeRoom(roomId)),
        getAllMessagesInRoom: (roomId) => dispatch(chatActions.getAllMessagesInRoom(roomId)),
    })
)(RoomListItem);
