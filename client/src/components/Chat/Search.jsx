import React from 'react'

export const Search = ({ filter, handleUserInput }) => {
    return (
        <div className="input-group input-group-lg">
            <input
                style={{ borderRadius: 0, borderTopLeftRadius: '0.25rem' }}
                type="text"
                className="form-control border-top-0 border-left-0 border-right-0 border-dark"
                placeholder="Search"
                name="filter"
                value={filter}
                onChange={handleUserInput}
            />
        </div>
    );
};
