import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './modal.css';

const ModalWindow = ({ handleClose, show, component: Component, otherProps }) => {
    const showHideClassName = show
        ? 'modal display-block'
        : 'modal display-none';

    return (
        <div className={showHideClassName + ' tabindex="-1" role="dialog"'}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-body">
                        <Component {...otherProps} />                
                    </div>
                    <div className="modal-footer">
                        <button
                            type="button"
                            className="btn btn-secondary"
                            data-dismiss="modal"
                            onClick={handleClose}
                        >
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default class Modal extends Component {
    static propTypes = {
        component: PropTypes.func,

        className: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = { show: false };
    }

    showModal = () => {
        this.setState({ show: true });
    };

    hideModal = () => {
        this.setState({ show: false });
    };

    render() {
        const { component, className, children, ...otherProps } = this.props;
        return (
            <div>
                <ModalWindow
                    show={this.state.show}
                    handleClose={this.hideModal}
                    component={component}
                    otherProps={otherProps}
                />
                <button
                    type="button"
                    className={className}
                    onClick={this.showModal}
                >
                    {children}
                </button>
            </div>
        );
    }
}
