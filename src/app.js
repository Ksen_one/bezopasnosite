import http from 'http';
import express from 'express'; // http server
import compression from 'compression'; // gzip compression. Need to install nginx in future;
import morgan from 'morgan'; // log requests
import bodyParser from 'body-parser';
import history from 'connect-history-api-fallback';
import io, { init as initSocket } from './core/socket';
import passport from './core/passport';
import initModels from './data/models'; // orm
import routes from './routes/routes';
import config from './config';
import logger from './core/logger';
import pathService from './services/pathService';

const app = express();
const server = http.createServer(app);


function initExpress() {
    if (config.devmode) {
        app.use(morgan('dev'));
    }
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json()); // get information from html forms

    app.use('/static', express.static(pathService.getRelative('client/build/static')));
    app.use(history({ index: pathService.getRelative('client/build/index.html') }));
    app.use('/static', express.static(pathService.getRelative('client/build/static')));


    app.use(compression());
}

function initErrorHandling() {
    // log unhandled errors
    app.use((err, req, res) => {
        res.status(500);
        res.render('error', { error: err });
    });

    process.on('uncaughtException', (err) => {
        logger.error(err);
    });
}

function start() {
    initExpress();

    routes.init(app);
    app.use(passport.initialize());

    initModels();
    initErrorHandling();

    io.attach(server);
    initSocket();

    server.listen(config.port, () => {
        console.log(`app listening on port ${config.port}`);
    });
}

start();
