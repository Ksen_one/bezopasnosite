import roomRepository from '../../data/repositories/roomRepository';
import controllerService from '../../services/controllerService';
import messageRepository from '../../data/repositories/messageRepository';

export default {
    async getAllRooms(req, res) {
        try {
            const rooms = await roomRepository.getRooms();
            return controllerService.sendData(rooms, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },

    async getUserRooms(req, res) {
        try {
            const { user } = req;
            if (!user) {
                throw new Error('no user');
            }
            const userRooms = await roomRepository.getRoomByUserId(user.id);
            return controllerService.sendData(userRooms, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },

    async createRoom(req, res) {
        try {
            const { user } = req;
            const { roomTitle: title } = req.body;
            if (!user) {
                throw new Error('no user');
            }
            const roomsWithSameTitle = await roomRepository.getRooms({ title });
            if (roomsWithSameTitle.length > 0) {
                throw new Error('room with same title already exists');
            }
            const newRoom = await roomRepository.createNewRoom(user.id, title);
            return controllerService.sendData(newRoom, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },

    async deleteRoom(req, res) {
        try {
            const { roomId } = req.body;

            await roomRepository.deleteRoomById(roomId);
            return controllerService.sendData(null, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },

    async getAllMessageInRoom(req, res) {
        try {
            const { roomId } = req.body;
            const messages = await messageRepository.getMessagesByRoomId(roomId);
            return controllerService.sendData(messages, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    }
};
