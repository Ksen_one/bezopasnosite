import pathService from '../../services/pathService';
import controllerService from '../../services/controllerService';

export default {
    async home(req, res) {
        try {
            return res.sendFile(pathService.getRelative('client/build/index.html'));
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    }
};
