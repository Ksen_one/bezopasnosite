import userRepository from '../../data/repositories/userRepository';
import controllerService from '../../services/controllerService';

export default {
    async getUsers(req, res) {
        try {
            const data = await userRepository.getUsers();
            return controllerService.sendData(data, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },

    async deleteUser(req, res) {
        try {
            const { id } = req.body;
            await userRepository.deleteUserById(id);
            return controllerService.sendData({}, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },

    async updateUserAccessLevel(req, res) {
        try {
            const { id, accessLevel } = req.body;
            const newUser = await userRepository.updateUser(id, { accessLevel });
            return controllerService.sendData(newUser, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    }
};
