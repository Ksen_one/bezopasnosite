import jwt from 'jsonwebtoken';
import config from '../../config';
import userRepository from '../../data/repositories/userRepository';
import controllerService from '../../services/controllerService';

export default {
    async login(req, res) {
        try {
            const { email, password } = req.body;
            const user = await userRepository.getUserByEmail(email);
            if (!user) {
                throw new Error('User with that email does not exist');
            }
            if (user.validatePassword(password)) {
                return jwt.sign(
                    {
                        id: user.id,
                        email: user.email,
                        username: user.username,
                        accessLevel: user.accessLevel
                    },
                    config.jwtSecret,
                    (err, token) => {
                        if (err) {
                            throw new Error(err);
                        }
                        return controllerService.sendData({ token, user }, res);
                    }
                );
            }
            return res.status(401).json({ status: 'unauthorised', error: 'Auth failed' });
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },

    async register(req, res) {
        try {
            const { email, username, password } = req.body;

            const emailCheck = await userRepository.getUserByEmail(email);
            if (emailCheck) {
                throw new Error('Email already taken');
            }
            const uniqueUsernameCheck = await userRepository.getUserByUsername(username);
            if (uniqueUsernameCheck) {
                throw new Error('Username already taken');
            }
            const user = await userRepository.createNewUser(email, username, password);
            return controllerService.sendData(user, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },

    async getCurrentUserData(req, res) {
        try {
            const { user } = req;
            if (!user) {
                throw new Error('No user data');
            }
            return controllerService.sendData(user, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    },
};
