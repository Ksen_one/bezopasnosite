import vulnerabilityRepository from '../../data/repositories/vulnerabilityRepository';
import controllerService from '../../services/controllerService';

export default {
    async getFilters(req, res) {
        try {
            const data = await vulnerabilityRepository.getVulnerabilityPropertiesNames();
            return controllerService.sendData(data, res);
        } catch (err) {
            return controllerService.sendFailureMessage(err, res);
        }
    }
};
