import initRouter from '../services/routerService';
import homeController from './controllers/homeController';
import vulnerabilityController from './controllers/vulnerabilityController';
import authController from './controllers/authController';
import filterController from './controllers/filterController';
import userController from './controllers/userController';
import chatController from './controllers/chatController';

let router = null;

function initRoutes() {
    router.get('/*', homeController.home);

    router.post('/api/login', authController.login);
    router.post('/api/register', authController.register);
    router.post('/api/getCurrentUserData', authController.getCurrentUserData, { accessLevel: 0 });

    router.post('/api/getUsers', userController.getUsers, { accessLevel: 1 });
    router.post('/api/deleteUser', userController.deleteUser, { accessLevel: 1 });
    router.post('/api/updateUserAccessLevel', userController.updateUserAccessLevel, { accessLevel: 1 });
    router.post('/api/getVulnerabilityById', vulnerabilityController.getVulnerabilityById, { accessLevel: 0 });
    router.post('/api/getVulnerabilities', vulnerabilityController.getVulnerabilities, { accessLevel: 0 });
    router.post('/api/getVulnerabilitiesCount', vulnerabilityController.getVulnerabilitesCount, { accessLevel: 0 });
    router.post('/api/getFilters', filterController.getFilters, { accessLevel: 0 });

    router.post('/api/getAllRooms', chatController.getAllRooms, { accessLevel: 0 });
    router.post('/api/getUserRooms', chatController.getUserRooms, { accessLevel: 1 });
    router.post('/api/createRoom', chatController.createRoom, { accessLevel: 1 });
    router.post('/api/deleteRoom', chatController.deleteRoom, { accessLevel: 1 });
    router.post('/api/getAllMessagesInRoom', chatController.getAllMessageInRoom, { accessLevel: 0 });
}

export default {
    init(app) {
        router = initRouter(app);

        initRoutes();
    },
};
