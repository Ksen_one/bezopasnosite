import winston from 'winston';
import _ from 'lodash';
import moment from 'moment';
import AppError from '../services/errorService';

import config from '../config';
import pathService from '../services/pathService';

let errorLogger = null;
let performanceLogger = null;
let infoLogger = null;
let performanceCache = {};

function initLoggers() {
    const getTransportFile = logFileName => new winston.transports.File({ filename: pathService.getDataRelative('logs', logFileName) });

    performanceLogger = winston.createLogger({
        transports: [
            getTransportFile('performance.log')
        ]
    });

    const {
        splat,
        combine,
        timestamp,
        printf,
        colorize
    } = winston.format;

    const format = ({ timestp, level, message, meta }) => `${timestp} [${level.toUpperCase()}] : ${message}; ${meta && Object.keys(meta).length ? `\n Error message: ${meta.errorMessage} \n Stack: ${meta.stack}` : ''}`;

    errorLogger = winston.createLogger({
        format: combine(
            colorize(),
            timestamp(),
            splat(),
            printf(format)
        )
    });

    switch (config.nodeEnv) {
    case 'production':
        errorLogger.add(new winston.transports.File({
            filename: getTransportFile('errors.log'),
            handleExceptions: true,
            level: 'warn'
        }));
        break;
    case 'test':
        break;
    default:
        errorLogger.add(new winston.transports.Console());
        errorLogger.exceptions.handle(new winston.transports.Console());
        break;
    }

    infoLogger = winston.createLogger({
        transports: [
            new winston.transports.Console(),
            getTransportFile('info.log')
        ]
    });
}

initLoggers();

export default {
    error(err) {
        if (_.isError(err)) {
            errorLogger.error('Error', { errorMessage: err.message, stack: err.stack });
            return;
        }
        errorLogger.error(err);
    },

    info(message) {
        infoLogger.info(message);
    },

    timeStart(timerName) {
        if (!config.devmode) return;
        if (performanceCache[timerName]) throw new AppError(`Timer was already created. Timer name: ${timerName}`);
        performanceCache[timerName] = new Date().getTime();
    },

    timeEnd(timerName) {
        if (!config.app.isDevLocal) return;
        if (!performanceCache[timerName]) throw new AppError(`Timer was not previously created. Timer name: ${timerName}`);

        const endTime = new Date().getTime();
        const startTime = performanceCache[timerName];

        const ms = endTime - startTime;
        performanceLogger.info(`Timer ${timerName} : ${moment.utc(ms).format('HH:mm:ss.SSS')}`);

        performanceCache = _.omit(performanceCache, timerName);
    },

    logMessage(message, metadata) {
        infoLogger.info(message, metadata);
    }
};
