import socketio from 'socket.io';
import jwt from 'jsonwebtoken';
import userRepository from '../data/repositories/userRepository';
import messageRepository from '../data/repositories/messageRepository';
import config from '../config';

const io = socketio({ wsEngine: 'ws' });

function init() {
    io.use((socket, next) => {
        if (socket.handshake.query && socket.handshake.query.token) {
            const { token } = socket.handshake.query;
            jwt.verify(token, config.jwtSecret, async (err, decodedToken) => {
                if (err) {
                    return next(new Error('Authentication error'));
                }
                try {
                    socket.user = await userRepository.getUserById(decodedToken.id);
                    return next();
                } catch (getUserError) {
                    return next(new Error(getUserError));
                }
            });
        } else {
            next(new Error('Authentication error'));
        }
    });

    io.on('connection', (socket) => {
        socket.on('clientMessage', async (data) => {
            try {
                const message = await messageRepository.createNewMessage(socket.user.id, data.currentRoom, data.message);
                message.dataValues.User = { username: socket.user.username };
                io.emit('serverMessage', { message });
            } catch (err) {
                console.log(err);
            }
        });

        socket.on('disconnect', () => {
            console.log('client disconnect...', socket.id);
        });

        socket.on('error', (err) => {
            console.log('received error from client:', socket.id);
            console.log(err);
        });
    });
}

export { init };
export default io;
