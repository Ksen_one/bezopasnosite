import passport from 'passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import userRepository from '../data/repositories/userRepository';
import config from '../config';

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.jwtSecret
};

const strategy = new Strategy(jwtOptions, async (jwtPayload, next) => {
    const { id } = jwtPayload;
    const user = await userRepository.getUserById(id);
    if (user) {
        next(null, user);
    } else {
        next(null, false);
    }
});

passport.use(strategy);

export default passport;
