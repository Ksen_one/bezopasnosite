export default {
    databaseUrl: process.env.DATABASE_URL || 'postgres://pguser:postgresroot@192.168.56.101:5432/pgdatabase',
    port: process.env.PORT || 5000,
    devmode: process.env.DEV_MODE || true,
    logErrors: process.env.LOG_ERRORS || true,
    nodeEnv: process.env.NODE_ENV,
    jwtSecret: 'super_secret_secret'
};
