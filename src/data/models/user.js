import DataTypes from 'sequelize';
import bcrypt from 'bcryptjs';
import Model from '../sequelize';

const User = Model.define(
    'User',
    {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true,
        },

        username: {
            type: DataTypes.STRING(50),
        },

        email: {
            type: DataTypes.STRING(255),
            validate: { isEmail: true },
        },

        password: {
            type: DataTypes.STRING(100),
            set(password) {
                this.setDataValue(
                    'password',
                    bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
                );
            },
        },

        accessLevel: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        }
    },
    {
        indexes: [{ fields: ['email'] }],
    }
);

User.prototype.toJSON = function toJSON() {
    const values = { ...this.get() };
    delete values.password;
    return values;
};

User.prototype.validatePassword = function validatePassword(password) {
    return bcrypt.compareSync(password, this.password);
};

User.prototype.isAdmin = function idAdmin() {
    return this.accessLevel === 1;
};

export default User;
