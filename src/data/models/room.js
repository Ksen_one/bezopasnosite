import DataTypes from 'sequelize';
import Model from '../sequelize';

const Room = Model.define(
    'Room',
    {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true,
        },

        title: {
            type: DataTypes.STRING(255),
        },
    }
);

export default Room;
