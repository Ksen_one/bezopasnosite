import DataTypes from 'sequelize';
import Model from '../sequelize';

const Message = Model.define(
    'Message',
    {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true,
        },

        text: {
            type: DataTypes.STRING(255),
        },

        date: {
            type: DataTypes.DATE,
            validate: {
                isDate: true
            },
            defaultValue: Model.NOW
        }
    }
);

export default Message;
