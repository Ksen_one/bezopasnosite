import Vulnerability from './vulnerability';
import User from './user';
import Message from './message';
import Room from './room';
import sequelize from '../sequelize';

User.hasMany(Message);
Message.belongsTo(User);

User.hasMany(Room);
Room.belongsTo(User);

Room.hasMany(Message);
Message.belongsTo(Room);

function sync(...args) {
    sequelize.sync(...args);
}

export default sync;
export {
    Vulnerability,
    User,
    Message,
    Room,
};
