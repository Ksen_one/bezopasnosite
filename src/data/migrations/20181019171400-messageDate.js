module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.addColumn('Message', 'date', {
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW'),
    }),

    down: queryInterface => queryInterface.removeColumn('Message', 'date'),
};
