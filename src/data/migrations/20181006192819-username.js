module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.addColumn('User', 'username', Sequelize.STRING),

    down: queryInterface => queryInterface.removeColumn('User', 'username'),
};
