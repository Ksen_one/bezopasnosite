module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.addColumn('User', 'accessLevel', {
        type: Sequelize.STRING,
        defaultValue: 0,
    }),

    down: queryInterface => queryInterface.removeColumn('User', 'accessLevel'),
};
