import Sequelize from 'sequelize';
import config from '../config';

const sequelize = new Sequelize(config.databaseUrl, {
    define: {
        freezeTableName: true,
        timestamps: false,
        underscoredAll: true
    },
});
sequelize.options.define.underscored = true;

export default sequelize;
