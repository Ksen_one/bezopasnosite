import { User } from '../models';

function getUsers() {
    return User.findAll({ order: ['id'] });
}

function getUserById(id) {
    return User.findById(id);
}

function getUserByEmail(email) {
    return User.findOne({ where: { email } });
}

function getUserByUsername(username) {
    return User.findOne({ where: { username } });
}

function createNewUser(email, username, password) {
    return User.create({ email, username, password });
}

function deleteUserById(id) {
    return User.findById(id).then(user => user.destroy());
}

function updateUser(id, options) {
    return User.findById(id).then(user => user.update(options));
}

export default {
    getUsers,
    getUserById,
    getUserByEmail,
    getUserByUsername,
    createNewUser,
    deleteUserById,
    updateUser,
};
