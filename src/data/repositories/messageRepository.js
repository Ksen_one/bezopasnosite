import { Message, User } from '../models';

function getMessages() {
    return Message.findAll({ order: ['date'] });
}

function getMessageById(id) {
    return Message.findById(id);
}

function getMessagesByUserId(userId) {
    return Message.findAll({
        where: { user_id: userId },
        order: ['date'],
        include: [User],
    });
}

function getMessagesByRoomId(roomId) {
    return Message.findAll({
        where: { room_id: roomId },
        order: ['date'],
        include: [{ model: User, attributes: ['username'] }],
    });
}

function createNewMessage(userId, roomId, text) {
    return Message.create({ user_id: userId, room_id: roomId, text });
}

function deleteMessageById(id) {
    return Message.findById(id).then(message => message.destroy());
}

export default {
    getMessages,
    getMessageById,
    getMessagesByUserId,
    getMessagesByRoomId,
    createNewMessage,
    deleteMessageById,
};
