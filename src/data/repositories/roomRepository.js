import { Room } from '../models';

function getRooms(where) {
    return Room.findAll({ order: ['id'], where });
}

function getRoomById(id) {
    return Room.findById(id);
}

function getRoomsByUserId(userId) {
    return Room.findAll({ where: { user_id: userId } });
}

function createNewRoom(userId, title) {
    return Room.create({ user_id: userId, title });
}

function deleteRoomById(id) {
    return Room.findById(id).then(room => room.destroy());
}

export default {
    getRooms,
    getRoomById,
    getRoomsByUserId,
    createNewRoom,
    deleteRoomById,
};
