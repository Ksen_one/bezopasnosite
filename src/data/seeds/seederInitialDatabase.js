import csvtojson from 'csvtojson';
import pathService from '../../services/pathService';
import { Vulnerability } from '../models';
import sequelize from '../sequelize';

async function seedData() {
    const dataPath = pathService.getRelative('db_seed.csv');
    const data = await csvtojson({
        delimiter: ';',
    }).fromFile(dataPath, { defaultEncoding: 'utf-8' });

    return sequelize.transaction().then((t) => {
        const promises = data.map((row, index) => Vulnerability.create(
            { id: index, ...row },
            { transaction: t }
        ));
        Promise.all(promises).then(() => {
            t.commit();
            console.log('DB seeded');
        });
    });
}

export default seedData;
