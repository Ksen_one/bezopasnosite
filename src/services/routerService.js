import passport from '../core/passport';

const authHandler = requestedAccess => (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user) => {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.status(401).send({ err: 'unauthorized' });
        }
        const { accessLevel } = user;
        if (requestedAccess > accessLevel) {
            return res.status(401).send({ err: 'unauthorized' });
        }
        req.user = user;
        return next();
    })(req, res, next);
};

export default function initRouter(app) {
    return {
        get(addr, handler, auth) {
            if (auth) {
                app.get.apply(app, [addr, authHandler(auth.accessLevel), handler]);
            }
            app.get.apply(app, [addr, handler]);
        },
        post(addr, handler, auth) {
            if (auth) {
                app.post.apply(app, [addr, authHandler(auth.accessLevel), handler]);
            }
            app.post.apply(app, [addr, handler]);
        },
    };
}
