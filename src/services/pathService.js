import path from 'path';

const rootPath = path.join(process.cwd());
const defaultDataPath = path.join(rootPath, 'data');

const getDataPath = () => (process.env.NODE_DATA_DIR ? process.env.NODE_DATA_DIR : defaultDataPath);

export default {
    path,
    getRelative(...paths) {
        paths.unshift(rootPath);
        return path.join.apply(this, paths);
    },
    getDataRelative(...paths) {
        paths.unshift(getDataPath());
        return path.join.apply(this, paths);
    },
};
