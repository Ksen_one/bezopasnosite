import logger from '../core/logger';

function sendData(data, res) {
    const response = {
        status: 'success',
        data,
    };
    res.send(response);
}

function sendFailureMessage(err, res) {
    logger.error(err);

    const response = {
        status: 'failure',
        error: err.message || err.name,
    };
    res.status(500).send(response);
}

export default {
    sendData,
    sendFailureMessage,
};
