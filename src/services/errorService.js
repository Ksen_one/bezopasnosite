import _ from 'lodash';

class AppError {
    message = '';
    uiShow = true;
    log = false;
    type = '';
    code = '';
    data;
    isAppError = true;

    constructor(...args) {
        Error.captureStackTrace(this, this.constructor);
        if (_.isString(args[0]) && _.isString(args[1])) { // signature type, code, options
            this.type = args[0];
            this.code = args[1];
            _.merge(this, args[2]);
        } else if (_.isString(args[0])) { // signature message, options
            this.message = args[0];
            _.merge(this, args[1]);
        } else {
            throw new Error('Unsupported AppError signature');
        }
    }
}

export default AppError;
